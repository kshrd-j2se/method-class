package org.kshrd.excercise_1;

public class MainCar {

    public static void main(String[] arg) {
        Car car = new Car(1, "Honda", "White", 2000, 150);
        car.showCarInformation();

        Car carDefault = new Car();
        carDefault.showCarInformation();

        Car car2 = new Car(2, "Suzuki", "Blue", 3000, 250);
        car2.showCarInformation();

        Car car3 = car2;
        car3.showCarInformation();


    }

}
