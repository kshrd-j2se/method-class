package org.kshrd.excercise_1;

public class Car {

    private int id, price, speed;
    private String brand, color;

    public Car() {}

    public Car(int id, String brand, String color, int price, int sp) {
        this.id = id;
        this.brand = brand;
        this.color = color;
        this.price = price;
        speed = sp; // because speed and sp are different name, so we don't need this keyword
    }

    public void showCarInformation() {
        System.out.println("Id: " + id);
        System.out.println("Brand: " + brand);
        System.out.println("Color: " + color);
        System.out.println("Price: " + price);
        System.out.println("Speed: " + speed);
    }


}
