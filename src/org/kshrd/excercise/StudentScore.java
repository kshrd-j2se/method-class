package org.kshrd.excercise;

public class StudentScore {

    public double average(double khmer, double math, double physic, double chemistry) {
        double total = khmer + math + physic + chemistry;
        double avg = total / 4;
        return avg;
    }




    public char grade(double avg) {
        char c = ' ';
        if (avg < 50)
            c = 'F';
         else if (avg < 60)
             c = 'E';
         else if (avg < 70)
             c = 'D';
         else if (avg <80)
             c = 'C';
         else if (avg <90)
             c = 'B';
         else
             c = 'A';

         return c;
    }

    public static int findTotal(String studentName, int ...numbers) {
        int total = 0;
        for (int num : numbers) {
            total = total + num;
        }
        return total;
    }

    public static void main(String[] args) {
        StudentScore studentScore = new StudentScore();

        double studentAverage = studentScore.average(60, 70, 90, 95);
        System.out.println("Employee Average is: " + studentAverage);

        char studentGrade = studentScore.grade(studentAverage);
        System.out.println("Employee grade is " + studentGrade);

        int number = findTotal("Sreypov",3, 4,5,6);

    }

}
