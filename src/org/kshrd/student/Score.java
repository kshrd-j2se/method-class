package org.kshrd.student;

public class Score {

    private static void myScore() {
        Student studentVar = new Student();
        studentVar.welcomeMessage();
        studentVar.defaultMethod();
        studentVar.protectedMethod();
    }


    public int sum(int a, int b) {
        int s = a + b;
        return s;
    }

    public int sum3Value(int a, int b, int c) {
        int s = a + b + c;
        return s;
    }

    public static void main (String[] ar) {
       // myScore();

        Score score = new Score();
        int mySum = score.sum(3, 4);
        System.out.println("==> sum = " + mySum);

        int mySum3Value = score.sum3Value(3,4,5);
        System.out.println("===> sum 3 value = " + mySum3Value);

    }


}
