package org.kshrd.student;

public class Student {

    public int age;



     public void welcomeMessage() {
        System.out.println("Hello KSHRD");
        privateMethod();
        defaultMethod();
        protectedMethod();
    }

    private void privateMethod() {
        System.out.println("This is a private method");
    }

    void defaultMethod() {
        System.out.println("This is a default method");
    }

    protected void protectedMethod() {
        System.out.println("This is a protected method");
    }


}
