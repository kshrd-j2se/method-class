package org.kshrd.class_creation;

public class Employee {

    private int id;
    private String name;

    public Employee() {

        System.out.println("Default constructor");
    }

    public Employee(int id, String name) {
       //this();
        this.id = id;
        this.name = name;
        //this(); // this not work
    }



}
